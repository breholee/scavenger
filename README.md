# Scavenger

This repository tracks upstream versions of Scavenger in respective branches:

- **xscavenger**: Scavenger, by David Ashley, for X Window systems
- **sdlscavenger**: SDL Scavenger, by Barry Mead and other contributors

The content of the **sdlscavenger** branch is slightly different from the
tarballs published on SourceForge. To match the **xscavenger** file hierarchy,
the source files and the Makefile remain in a `src` directory (the `Makefile`
and `names.h` files are updated accordingly). The generated binary files
have been removed. Some documentation files and unused artwork are merged
or simply kept.

New developments from the SDL Scavenger branch are done in the **current**
branch.
